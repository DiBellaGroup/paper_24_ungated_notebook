function create_dicoms(sliceNumber, dataDate, resultsDate)
  % Function gives important information such as location of template Dicom
  % then attaches DICOM headers to the source data.

  % Inputs help find data relative to dated path.
  % dataDate = '2015-12-09';
  % resultsDate = '2015-12-31';


  % Set variables that never change
  Matlab.path = ['results/' resultsDate];
  Dicom.inputPath = '''/v/raid1a/gadluru/MRIdata/Cardiac/Verio/Repeatability/P092214/ReconData/CV_Radial_Dec2012_ungated_repeatability_dryRun(86006)/''';

  % Iterate through the 4 methods and each slice
  interpMethodName = {'grid3', 'grog', 'nn', 'nufft'};
  seriesNumber = sliceNumber * 10;

  for methodName = interpMethodName
    % Iteration variables
    name = methodName{1};
    seriesNumber = seriesNumber + 1;

    % Create structs for Shared method
    Matlab.filename = ['24_ray_slice_' sliceNumber '_' name '.mat'];
    Matlab.varName = [name 'Image'];
    Dicom.outputPath = [name '_dicoms/slice_' sliceNumber];
    Dicom.description = [name '-Slice' sliceNumber];

    otherArgs = [' -i range_01 data/' dataDate ...
                 '/listofselectedFrames_roi_sum_dd_Nearsystolic_MID_52_slice_' ...
                 sliceNumber '.mat'...
                 ' -o series_number_increment ' num2str(seriesNumber)];

    Shared.create_dicoms(Matlab, Dicom, otherArgs)
  end
end
