disp('Checking notebook dependencies')

% State package manager version then import if OK
PackageManagement.verify_self('2')
import PackageManagement.*

% Add package dependencies here
verify_package('Shared', '2');
