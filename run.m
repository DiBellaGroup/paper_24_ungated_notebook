% Clear everything out
clear all
close all

% Add all files local to notebook and packages
addpath('..')
addpath('lib')
addpath('~/packages')

% Verify that needed packages are available
verify

% pick the date folder to get data from
dataDate = '2015-12-09';

% Pick which slices you want to reconstruct
slicesRange = [1 3];

% create the folder for results as needed
todaysDate = Shared.todays_date;
mkdir('results', todaysDate)

% Go through desired slices and process
for iSlice = slicesRange
  sliceString = num2str(iSlice);

  % Load data
  file = ['data/' dataDate '/24ray_ungated_systolic_slice_' sliceString '.mat'];
  load(file)

  % Reconstruct and reorient
  Results = Shared.reconstruct_data(kSpaceSlice, trajectory, iSlice);
  Results.grid3Image = rot90(Results.grid3Image, 2);
  Results.grogImage = rot90(Results.grogImage, 2);
  Results.nnImage = rot90(Results.nnImage, 2);
  Results.nufftImage = rot90(Results.nufftImage, 2);

  % Scale to what region of interest?
  roiRows = 125:183;
  roiCols = 100:165;
  Results = Shared.scale_results(Results, roiRows, roiCols, '24_ray');

  % Write raw data
  resultsPath = ['results/' todaysDate '/24ray_slice' sliceString '_results.mat'];
  save(resultsPath, 'Results')
  % Shared.plot_results(Results, sliceString);

  Write DICOMS
  create_dicoms(sliceString, dataDate, todaysDate);
end
